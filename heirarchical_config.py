from os import PathLike
import yaml
from typing import Any, Dict, List, Literal, Tuple, Union
from transformers import TrainingArguments

MAX_LENGTH = 256


class HeirarchicalModelConfig:
    def __init__(self):
        self._existence_model_training_args = None
        self._sentiment_model_training_args = None
        self._emotion_model_training_args = None
        self.tokenizer: Any = None

        self._existence_data_train: Tuple[List[str], List[int]] = ([], [])
        self._existence_data_validate: Tuple[List[str], List[int]] = ([], [])
        self._sentiment_data_train: Tuple[List[str], List[int]] = ([], [])
        self._sentiment_data_validate: Tuple[List[str], List[int]] = ([], [])
        self._emotion_data_train: Tuple[List[str], List[int]] = ([], [])
        self._emotion_data_validate: Tuple[List[str], List[int]] = ([], [])
        self.sentic_scores_train: List[float] = []
        self.sentic_scores_validate: List[float] = []

        self.max_length = MAX_LENGTH
        self.learning_rate = 0.01
        self.apply_preprocessing = True
        self.mlflow_args: Dict[Literal["tracking uri", "experiment name"], Any] = {}
        self.callbacks: Dict[str, Any] = {}
        self.preprocessing_settings: Dict[
            Literal["segmentation", "remove_link", "replace_emoji", "remove_username"], bool
        ] = {"segmentation": True, "remove_link": True, "replace_emoji": True, "remove_username": True}
        self.sentic_computing = False

    @classmethod
    def from_file(cls, path: Union[str, PathLike]):
        with open(path, "r") as file:
            contents = file.read()

        config = yaml.safe_load(contents)

        existence_model_args = config["existence model"] if "existence model" in config else None
        sentiment_model_args = config["sentiment model"] if "sentiment model" in config else None
        emotion_model_args = config["emotion model"] if "emotion model" in config else None

        training_params = config["train params"]
        this = cls()
        if not existence_model_args and not sentiment_model_args and not emotion_model_args:
            this.emotion_model_training_args = (
                this.sentiment_model_training_args
            ) = this.existence_model_training_args = TrainingArguments(
                output_dir=training_params["model save dir"],
                per_device_train_batch_size=training_params["train batch size"],
                per_device_eval_batch_size=training_params["validation batch size"],
                num_train_epochs=training_params["epochs"],
                eval_accumulation_steps=training_params["steps to move to cpu"],
                evaluation_strategy=training_params["checkpoint interval strategy"],
                report_to="all",
                save_strategy=training_params["checkpoint interval strategy"],
                save_total_limit=training_params["max checkpoints"],
                metric_for_best_model=training_params["optimization metric"],
                load_best_model_at_end=training_params["load best model at end"],
                learning_rate=config["optimizer"]["learning rate"],
            )
        this.learning_rate = config["optimizer"]["learning rate"]
        this.apply_preprocessing = training_params["apply preprocessing"]
        if "mlflow" in config:
            this.mlflow_args = config["mlflow"]
        if "callbacks" in config:
            this.callbacks = config["callbacks"]
        if "preprocessing" in config:
            this.preprocessing_settings = config["preprocessing"]
        if "sentic computing" in config and config["sentic computing"]:
            this.sentic_computing = True
        return this

    @property
    def existence_data_train(self):
        return self._existence_data_train

    @existence_data_train.setter
    def existence_data_train(self, value):
        assert len(value) == 2, f"Cannot assign a tuple of {len(value)} elements to the data for existence model"
        try:
            iter(value[0])
            iter(value[1])
            self._existence_data_train = (value[0].tolist(), value[1].tolist())
        except TypeError:
            print("One of the provided values of the tuple is not iterable")

    @property
    def existence_data_validate(self):
        return self._existence_data_validate

    @existence_data_validate.setter
    def existence_data_validate(self, value):
        assert len(value) == 2, f"Cannot assign a tuple of {len(value)} elements to the data for existence model"
        try:
            iter(value[0])
            iter(value[1])
            self._existence_data_validate = (value[0].tolist(), value[1].tolist())
        except TypeError:
            print("One of the provided values of the tuple is not iterable")

    @property
    def sentiment_data_train(self):
        return self._sentiment_data_train

    @sentiment_data_train.setter
    def sentiment_data_train(self, value):
        assert len(value) == 2, f"Cannot assign a tuple of {len(value)} elements to the data for sentiment model"
        try:
            iter(value[0])
            iter(value[1])
            self._sentiment_data_train = (value[0].tolist(), value[1].tolist())
        except TypeError:
            print("One of the provided values of the tuple is not iterable")

    @property
    def sentiment_data_validate(self):
        return self._sentiment_data_validate

    @sentiment_data_validate.setter
    def sentiment_data_validate(self, value):
        assert len(value) == 2, f"Cannot assign a tuple of {len(value)} elements to the data for sentiment model"
        try:
            iter(value[0])
            iter(value[1])
            self._sentiment_data_validate = (value[0].tolist(), value[1].tolist())
        except TypeError:
            print("One of the provided values of the tuple is not iterable")

    @property
    def emotion_data_train(self):
        return self._emotion_data_train

    @emotion_data_train.setter
    def emotion_data_train(self, value):
        assert len(value) == 2, f"Cannot assign a tuple of {len(value)} elements to the data for emotion model"
        try:
            iter(value[0])
            iter(value[1])
            self._emotion_data_train = (value[0].tolist(), value[1].tolist())
        except TypeError:
            print("One of the provided values of the tuple is not iterable")

    @property
    def emotion_data_validate(self):
        return self._emotion_data_validate

    @emotion_data_validate.setter
    def emotion_data_validate(self, value):
        assert len(value) == 2, f"Cannot assign a tuple of {len(value)} elements to the data for emotion model"
        try:
            iter(value[0])
            iter(value[1])
            self._emotion_data_validate = (value[0].tolist(), value[1].tolist())
        except TypeError:
            print("One of the provided values of the tuple is not iterable")

    @property
    def existence_model_training_args(self):
        return self._existence_model_training_args

    @existence_model_training_args.setter
    def existence_model_training_args(self, args):
        if not isinstance(args, TrainingArguments):
            raise ValueError("training arguments must be of type TrainingArguments")

        self._existence_model_training_args = args

    @property
    def sentiment_model_training_args(self):
        return self._sentiment_model_training_args

    @sentiment_model_training_args.setter
    def sentiment_model_training_args(self, args):
        if not isinstance(args, TrainingArguments):
            raise ValueError("training arguments must be of type TrainingArguments")

        self._sentiment_model_training_args = args

    @property
    def emotion_model_training_args(self):
        return self._emotion_model_training_args

    @emotion_model_training_args.setter
    def emotion_model_training_args(self, args):
        if not isinstance(args, TrainingArguments):
            raise ValueError("training arguments must be of type TrainingArguments")

        self._emotion_model_training_args = args
