import math
from emotion_model import ExistenceModel
from heirarchical_config import HeirarchicalModelConfig
from dataset import Triage
from torch.utils.data import DataLoader
from transformers import (
    Trainer,
    AutoModelForSequenceClassification,
    EarlyStoppingCallback,
)
import torch
import tempfile
from torch.optim import AdamW
from torch import cuda, tensor, nn, argmax, no_grad, zeros
from transformers.modeling_outputs import SequenceClassifierOutput
import numpy as np
import mlflow
import os
from rich.progress import Progress, MofNCompleteColumn, TimeElapsedColumn
from time import time
from callbacks import CustomEarlyStopping
from utils import (
    SaveBestModel,
    backpropagate,
    log_metrics,
    log_params,
    move_to_device,
    save_model_and_data,
    compute_binary_metrics,
    compute_multiclass_metrics,
    predict_from_model,
)
from pytorch_model_summary import summary

EARLY_STOP_CONFIG = {"min_delta": 0.0005, "patience": 3}
ces = CustomEarlyStopping(mode="max", min_delta=EARLY_STOP_CONFIG["min_delta"], patience=EARLY_STOP_CONFIG["patience"])

OPTIMIZATION_METRIC = "f1"


class HierarchicalModel:
    def __init__(
        self,
        config: HeirarchicalModelConfig,
        existence_model,
        sentiment_model,
        emotion_model,
    ):

        self._existence_model_training_args = config.existence_model_training_args
        self._sentiment_model_training_args = config.sentiment_model_training_args
        self._emotion_model_training_args = config.emotion_model_training_args

        self.existence_model_trainer = self.sentiment_model_trainer = self.emotion_model_trainer = None

        self.tokenizer = config.tokenizer
        self.existence_model = existence_model
        self.sentiment_model = sentiment_model
        self.emotion_model = emotion_model

        self.config = config
        self._encode_existence_dataset()
        self._encode_sentiment_dataset()
        self._encode_emotion_dataset()
        self._init_existence_trainer()
        self._init_sentiment_trainer()
        self._init_emotion_trainer()

        self.available_device()

    def available_device(self):
        cuda_available = cuda.is_available()
        if cuda_available:
            self.device = "cuda"
        else:
            self.device = "cpu"

    def _encode_existence_dataset(self):
        encodings_train = self.tokenizer.batch_encode_plus(
            self.config.existence_data_train[0],
            max_length=self.config.max_length,
            padding="max_length",
            return_token_type_ids=True,
            truncation=True,
        )
        print("Encoding existence training data")
        training_set = Triage(
            encodings_train["input_ids"], encodings_train["attention_mask"], self.config.existence_data_train[1]
        )
        encodings_test = self.tokenizer.batch_encode_plus(
            self.config.existence_data_validate[0],
            max_length=self.config.max_length,
            padding="max_length",
            return_token_type_ids=True,
            truncation=True,
        )
        print("Encoding existence testing data")
        testing_set = Triage(
            encodings_test["input_ids"], encodings_test["attention_mask"], self.config.existence_data_validate[1]
        )
        b_size = 32
        if self._existence_model_training_args is not None:
            b_size = self._existence_model_training_args.per_device_train_batch_size
        self.existence_data_train = DataLoader(training_set, batch_size=b_size, shuffle=True, num_workers=0)
        self.existence_data_validate = DataLoader(testing_set, batch_size=b_size, shuffle=True, num_workers=0)

    def _encode_sentiment_dataset(self):
        encodings_train = self.tokenizer.batch_encode_plus(
            self.config.sentiment_data_train[0],
            max_length=self.config.max_length,
            padding="max_length",
            return_token_type_ids=True,
            truncation=True,
        )
        print("Encoding senitment training data")
        training_set = Triage(
            encodings_train["input_ids"], encodings_train["attention_mask"], self.config.sentiment_data_train[1]
        )
        encodings_test = self.tokenizer.batch_encode_plus(
            self.config.sentiment_data_validate[0],
            max_length=self.config.max_length,
            padding="max_length",
            return_token_type_ids=True,
            truncation=True,
        )
        print("Encoding senitment training data")
        testing_set = Triage(
            encodings_test["input_ids"], encodings_test["attention_mask"], self.config.sentiment_data_validate[1]
        )
        b_size = 32
        if self._sentiment_model_training_args is not None:
            b_size = self._sentiment_model_training_args.per_device_train_batch_size
        self.sentiment_data_train = DataLoader(training_set, batch_size=b_size, shuffle=True, num_workers=0)
        self.sentiment_data_validate = DataLoader(testing_set, batch_size=b_size, shuffle=True, num_workers=0)

    def _encode_emotion_dataset(self):
        encodings_train = self.tokenizer.batch_encode_plus(
            self.config.emotion_data_train[0],
            max_length=self.config.max_length,
            padding="max_length",
            return_token_type_ids=True,
            truncation=True,
        )
        print("Encoding emotion training data")
        if self.config.sentic_computing:
            training_set = Triage(
                encodings_train["input_ids"],
                encodings_train["attention_mask"],
                self.config.emotion_data_train[1],
                self.config.sentic_scores_train,
            )
        else:
            training_set = Triage(
                encodings_train["input_ids"], encodings_train["attention_mask"], self.config.emotion_data_train[1]
            )
        encodings_test = self.tokenizer.batch_encode_plus(
            self.config.emotion_data_validate[0],
            max_length=self.config.max_length,
            padding="max_length",
            return_token_type_ids=True,
            truncation=True,
        )
        print("Encoding emotion testing data")
        if self.config.sentic_computing:
            testing_set = Triage(
                encodings_test["input_ids"],
                encodings_test["attention_mask"],
                self.config.emotion_data_validate[1],
                self.config.sentic_scores_validate,
            )
        else:
            testing_set = Triage(
                encodings_test["input_ids"], encodings_test["attention_mask"], self.config.emotion_data_validate[1]
            )

        b_size = 32
        if self._emotion_model_training_args is not None:
            b_size = self._emotion_model_training_args.per_device_train_batch_size
        self.emotion_data_train = DataLoader(training_set, batch_size=b_size, shuffle=True, num_workers=0)
        self.emotion_data_validate = DataLoader(testing_set, batch_size=b_size, shuffle=True, num_workers=0)

    @classmethod
    def from_pretrained(cls, existence_model_path, sentiment_model_path, emotion_model_path):
        existence_model = AutoModelForSequenceClassification.from_pretrained(existence_model_path)
        sentiment_model = AutoModelForSequenceClassification.from_pretrained(sentiment_model_path)
        emotion_model = AutoModelForSequenceClassification.from_pretrained(emotion_model_path)
        config = HeirarchicalModelConfig()
        return cls(
            config, existence_model=existence_model, sentiment_model=sentiment_model, emotion_model=emotion_model
        )

    def _init_existence_trainer(self):
        if self._existence_model_training_args is not None:
            self.existence_model_trainer = Trainer(
                model=self.existence_model,
                args=self._existence_model_training_args,
                train_dataset=self.existence_data_train,
                eval_dataset=self.existence_data_validate,
                tokenizer=self.tokenizer,
                compute_metrics=compute_binary_metrics,
            )
            es = EarlyStoppingCallback(EARLY_STOP_CONFIG["patience"], EARLY_STOP_CONFIG["min_delta"])
            self.existence_model_trainer.add_callback(es)

    def _init_sentiment_trainer(self):
        if self._sentiment_model_training_args is not None:
            self.sentiment_model_trainer = Trainer(
                model=self.sentiment_model,
                args=self._sentiment_model_training_args,
                train_dataset=self.sentiment_data_train,
                eval_dataset=self.sentiment_data_validate,
                tokenizer=self.tokenizer,
                compute_metrics=compute_multiclass_metrics,
            )
            es = EarlyStoppingCallback(EARLY_STOP_CONFIG["patience"], EARLY_STOP_CONFIG["min_delta"])
            self.sentiment_model_trainer.add_callback(es)

    def _init_emotion_trainer(self):
        if self._emotion_model_training_args is not None:
            self.emotion_model_trainer = Trainer(
                model=self.emotion_model,
                args=self._emotion_model_training_args,
                train_dataset=self.config.emotion_data_train,
                eval_dataset=self.config.emotion_data_validate,
                tokenizer=self.tokenizer,
                compute_metrics=compute_multiclass_metrics,
            )
            # es = EarlyStoppingCallback(3, 0.05)
            # self.emotion_model_trainer.add_callback(es)

    def _predict_existence(self, encodings):
        # dataset = Triage(encodings["input_ids"], encodings["attention_mask"])
        # if self.existence_model_trainer is None:
        #     self.existence_model_trainer = Trainer(
        #         model=self.existence_model, args=TrainingArguments(output_dir="./results", report_to="all")
        #     )

        # output = self.existence_model_trainer.predict(dataset)  # type: ignore
        model: nn.Module = self.existence_model
        ids, mask, targets, sentic_value = move_to_device(encodings, self.device)

        outputs = model(ids, mask)
        pred_prob = nn.Softmax(dim=1)(tensor(outputs.data))
        softmaxed_prob = pred_prob.cpu().numpy()

        has_emotion_labels = np.argmax(softmaxed_prob, axis=1)
        return has_emotion_labels

    def _predict_sentiment(self, encodings):
        # polarity_dataset = Triage(encodings["input_ids"], encodings["attention_mask"])
        # if self.sentiment_model_trainer is None:
        #     self.sentiment_model_trainer = Trainer(
        #         model=self.sentiment_model, args=TrainingArguments(output_dir="./results", report_to="all")
        #     )

        model: nn.Module = self.sentiment_model
        ids, mask, targets, sentic_value = move_to_device(encodings, self.device)

        outputs = model(ids, mask)

        pred_prob = nn.Softmax(dim=1)(tensor(outputs.data))

        softmaxed_prob = pred_prob.cpu().numpy()

        # 0 to 1 intensity aka probability of a label
        polarity_intensity = np.max(softmaxed_prob, axis=1)
        # transform the probabilities into label. Get label with max probability
        polarity_labels = np.argmax(softmaxed_prob, axis=1)
        # decode label into -1, 0, 1 using encoder

        def mapper(x):
            if x == 0:
                return 0
            if x == 1:
                return -1
            if x == 2:
                return 1

        vmapper = np.vectorize(mapper)
        polarity_labels = vmapper(polarity_labels)

        return polarity_intensity * polarity_labels

    def _predict(self, encodings):
        return self._predict_existence(encodings) * self._predict_sentiment(encodings)

    def to(self, device: str):
        if device == "cuda":
            cuda_available = cuda.is_available()
            if cuda_available:
                self.existence_model.cuda()
                self.sentiment_model.cuda()
                self.emotion_model.cuda()
            else:
                print("Cuda unavailable")

    def train(self, model_name: str):
        print("=" * 100)
        print("                                             Training Existence Model")
        print("=" * 100)
        print(summary(self.existence_model, torch.zeros((1, 256), dtype=torch.int64).to("cuda"), show_input=True))
        assert self.existence_model_trainer is not None
        with mlflow.start_run(run_name=model_name, tags={"type": "existence"}, nested=True):
            log_params(self.existence_model_trainer, log_basemodel_params=True)
            self.setup_progress(
                self.existence_model,
                self.existence_data_train,
                self.existence_data_validate,
                self._existence_model_training_args,
                "existence",
            )
            save_model_and_data(self.existence_model_trainer.model, "existence_model", 7)

        cuda.empty_cache()

        print("=" * 100)
        print("                                             Training Sentiment Model")
        print("=" * 100)
        print(summary(self.sentiment_model, torch.zeros((1, 256), dtype=torch.int64).to("cuda"), show_input=True))
        assert self.sentiment_model_trainer is not None
        with mlflow.start_run(run_name=model_name, tags={"type": "sentiment"}, nested=True):
            log_params(self.sentiment_model_trainer, log_basemodel_params=True)
            self.setup_progress(
                self.sentiment_model,
                self.sentiment_data_train,
                self.sentiment_data_validate,
                self._sentiment_model_training_args,
                "sentiment",
            )
            save_model_and_data(self.sentiment_model_trainer, "sentiment_model", 3)

        cuda.empty_cache()

        print("=" * 100)
        print("                                             Training Emotion Model")
        print("=" * 100)
        print(summary(self.emotion_model, torch.zeros((1, 256), dtype=torch.int64).to("cuda"), show_input=True))

        assert self.emotion_model_trainer is not None
        with mlflow.start_run(run_name=model_name, tags={"type": "emotion"}, nested=True):
            self.existence_model.eval()
            self.sentiment_model.eval()
            log_params(self.emotion_model_trainer, log_basemodel_params=True)
            self.setup_progress(
                self.emotion_model,
                self.emotion_data_train,
                self.emotion_data_validate,
                self._emotion_model_training_args,
                "emotion",
            )
            save_model_and_data(self.emotion_model_trainer.model, "emotion_model", 7)

    def setup_progress(self, model: torch.nn.Module, train_data, validation_data, training_args, model_type):
        global_steps = 0
        num_epochs = training_args.num_train_epochs
        batch_size = training_args.per_device_train_batch_size
        total_steps = math.ceil((len(train_data.dataset) / batch_size) * num_epochs)
        progress = Progress(
            *Progress.get_default_columns(),
            MofNCompleteColumn(),
            TimeElapsedColumn(),
        )
        save_best_model = SaveBestModel(model_type=model_type, optimization_metric="f1")
        best_model_path = ""
        with progress:
            training_bar = progress.add_task("[blue]Training...", total=total_steps)
            for epoch in range(int(num_epochs)):
                global_steps += self.train_model(
                    model, train_data, epoch, global_steps, progress, training_bar, training_args, model_type
                )
                metrics = self.evaluate_model(model, validation_data, global_steps, progress, training_args, model_type)
                # save best model
                best_model_path = save_best_model(metrics=metrics, epoch=epoch, model=model)
                # Early stop callback
                if ces.step(metrics[OPTIMIZATION_METRIC]) and epoch >= (0.4 * num_epochs):
                    print(f"Stopping training due to the metric {OPTIMIZATION_METRIC} not changing more than min_delta")
                    break
            # loading best model
            if best_model_path:
                checkpoint = torch.load(best_model_path, map_location="cpu")
                print(
                    f"Loading best model for epoch {checkpoint['epoch']} with metric value {checkpoint['optimization_metric']}"
                )
                model.load_state_dict(checkpoint["model_state_dict"])

    def train_model(
        self, model, train_data, epoch, global_steps, progress_context, progress_bar, training_args, model_type
    ):
        # Creating the loss function and optimizer
        loss_function = torch.nn.CrossEntropyLoss()
        optimizer = torch.optim.Adam(params=model.parameters(), lr=self.config.learning_rate)

        epoch_loss = 0
        steps = 0
        nb_tr_examples = 0
        all_labels, all_pred = [], []
        model.train()

        start_time = time()
        for _, data in enumerate(train_data, 0):
            ids, mask, targets, sentic_value = move_to_device(data, self.device)

            if not isinstance(model, ExistenceModel):
                tier1_predictions = self._predict(data)
                tier1_predictions = tensor(tier1_predictions).to(self.device)

                big_idx, loss = predict_from_model(
                    model, loss_function, targets, ids, mask, tier1_predictions, sentic_value
                )
            else:
                big_idx, loss = predict_from_model(model, loss_function, targets, ids, mask)

            epoch_loss += loss.item()

            steps += 1
            progress_context.update(progress_bar, advance=1)
            nb_tr_examples += targets.size(0)

            backpropagate(loss=loss, optimizer=optimizer)

            all_pred.extend(big_idx.tolist())
            all_labels.extend(targets.tolist())

            if steps % 500 == 0:
                epoch_to_log = epoch + round(float(nb_tr_examples / len(train_data.dataset)), 2)
                mlflow.log_metric("epoch", epoch_to_log, step=global_steps)

        if epoch == training_args.num_train_epochs - 1:  # last epoch
            epoch_to_log = epoch + round(float(nb_tr_examples / len(train_data.dataset)), 2)
            mlflow.log_metric("epoch", epoch_to_log, step=global_steps)

        epoch_loss = epoch_loss / steps
        if model_type == "existence":
            metrics = compute_binary_metrics((all_pred, all_labels))
        else:
            metrics = compute_multiclass_metrics((all_pred, all_labels), prob_based=False)

        metrics["steps_per_second"] = steps
        log_metrics(metrics, epoch_loss, start_time, nb_tr_examples, global_steps, prefix="train_")
        return steps

    def evaluate_model(self, model, eval_data, global_steps, progress_context, training_args, model_type):
        loss_function = torch.nn.CrossEntropyLoss()

        model.eval()
        epoch_loss = 0
        steps = 0
        nb_tr_examples = 0
        all_labels, all_pred = [], []
        start_time = time()

        assert training_args is not None
        batch_size = training_args.per_device_eval_batch_size
        total_steps = math.ceil((len(eval_data.dataset) / batch_size))

        eval_bar = progress_context.add_task("[green]Evaluating...", total=total_steps)
        with torch.no_grad():
            for _, data in enumerate(eval_data, 0):
                ids, mask, targets, sentic_value = move_to_device(data, self.device)

                if not isinstance(model, ExistenceModel):
                    tier1_predictions = self._predict(data)
                    tier1_predictions = tensor(tier1_predictions).to(self.device)

                    big_idx, loss = predict_from_model(
                        model, loss_function, targets, ids, mask, tier1_predictions, sentic_value
                    )
                else:
                    big_idx, loss = predict_from_model(model, loss_function, targets, ids, mask)

                epoch_loss += loss

                steps += 1
                progress_context.update(eval_bar, advance=1)
                nb_tr_examples += targets.size(0)

                all_pred.extend(big_idx.tolist())
                all_labels.extend(targets.tolist())

        progress_context.remove_task(eval_bar)
        epoch_loss = epoch_loss / steps
        if model_type == "existence":
            metrics = compute_binary_metrics((all_pred, all_labels))
        else:
            metrics = compute_multiclass_metrics((all_pred, all_labels), prob_based=False)

        metrics["steps_per_second"] = steps
        print(log_metrics(metrics, epoch_loss, start_time, nb_tr_examples, global_steps, prefix="eval_"))

        return metrics
