import re
from nltk import ngrams
import pickle
import nltk
from numpy import argmax
from nltk.corpus import sentiwordnet as swn
from nltk.corpus import wordnet
from nltk.tag import pos_tag
from nltk.stem import WordNetLemmatizer
from senticnet.senticnet import SenticNet

with open("roman_sentic_net.pkl", "rb") as file:
    bsn = pickle.load(file)

with open("hswn.pkl", "rb") as file:
    hswn = pickle.load(file)

sn = SenticNet()


def get_wordnet_pos(word):
    tag = nltk.pos_tag([word])[0][1][0].upper()
    tag_dict = {"J": wordnet.ADJ, "N": wordnet.NOUN, "V": wordnet.VERB, "R": wordnet.ADV}
    return tag_dict.get(tag, wordnet.NOUN)


def get_sentiment_score(word):
    # 1. Tokenize
    sentiment_score = 0.0

    lemmatizer = WordNetLemmatizer()

    tag = get_wordnet_pos(word)
    item_res = lemmatizer.lemmatize(word, tag)
    if not item_res:
        return

    synsets = wordnet.synsets(item_res, pos=tag)
    if len(synsets) == 0:
        print("Nope!", word)
        return

    # Take the first, the most common
    synset = synsets[0]
    swn_synset = swn.senti_synset(synset.name())
    sentiment_score += swn_synset.pos_score() - swn_synset.neg_score()

    return sentiment_score


def sentic_computing_score(text):
    text = text.strip()
    if len(text) == 0:
        return 0

    grams = []
    for i in range(5):
        grams.extend(list(ngrams(text.split(), i + 1)))

    combinations = []

    for item in grams:
        combinations.append(" ".join(item))

    combinations.sort(reverse=True, key=len)
    processed_tokens = []

    for c in combinations:
        try:
            new_str = text.replace(c, "::", 1)
            (left, right) = new_str.split("::")
        except:
            print(text)
        # checking in babel sentic net
        if c.strip() in bsn and c.strip() not in processed_tokens:
            val = bsn[c][6] + sentic_computing_score(left) + sentic_computing_score(right)
            processed_tokens.append(c)
            return val
        # checking in sentic net
        elif c.strip() in sn.data and c.strip() not in processed_tokens:
            val = float(sn.data[c][7]) + sentic_computing_score(left) + sentic_computing_score(right)
            processed_tokens.append(c)
            return val
        # elif len(c.strip().split()) == 1 and c.strip() not in processed_tokens:
        #     score = get_sentiment_score(c) or 0
        #     val = score + sentic_computing_score(left) + sentic_computing_score(right)
        #     processed_tokens.append(c.strip())
        #     return val
        # checking in Hindi senti word net
        elif c.strip() in hswn and c.strip() not in processed_tokens:
            if argmax(hswn[c]) == 1:  # negative sentiment
                score = -1 * max(hswn[c])
            else:
                score = max(hswn[c])

            val = score + sentic_computing_score(left) + sentic_computing_score(right)
            processed_tokens.append(c)
            return val

    return 0


sentic_computing_score("@eshita2428 @ColorsTV @BiggBoss Salute to your #Hope 😀😂😂😂😂😂😂😂😂😂")
