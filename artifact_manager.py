import mlflow
from transformers import Trainer
from os import path, makedirs
import torch
from typing import Union


class ArtifactManager(mlflow.pyfunc.PythonModel):
    model = None

    def save_model(self, trainer: Union[Trainer, torch.nn.Module], model_type: str):
        active_run = mlflow.active_run()
        if active_run is not None:
            artifact_uri = active_run.info.artifact_uri
            file_path = path.join(artifact_uri, model_type)
            if isinstance(trainer, Trainer):
                trainer.save_model(file_path)
            elif isinstance(trainer, torch.nn.Module):
                makedirs(file_path, exist_ok=True)
                file_path = path.join(file_path, "model_dict.pth")
                torch.save(trainer, file_path)

    def load_context(self, context):
        pass
