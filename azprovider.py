# azureml-core of version 1.0.72 or higher is required
# azureml-dataprep[pandas] of version 1.1.34 or higher is required
from azureml.core import Workspace, Dataset

subscription_id = "65d2650c-2f0b-4060-bedc-54d291c87ba5"
resource_group = "ML_RG"
workspace_name = "mlwork"


def connect(dataset):
    workspace = Workspace(subscription_id, resource_group, workspace_name)

    dataset = Dataset.get_by_name(workspace, name=dataset)
    data = dataset.to_pandas_dataframe()
    return data
