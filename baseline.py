from sklearn.model_selection import train_test_split
import pandas as pd
from torch import zeros
import torch
from dataset import Triage
import os
import tempfile
import mlflow
from heirarchical_config import HeirarchicalModelConfig
from torch.utils.data import DataLoader
from utils import compute_multiclass_metrics

from transformers import (
    Trainer,
    AutoModelForSequenceClassification,
    AutoTokenizer,
)
from bsn import sentic_computing_score
from pytorch_model_summary import summary
from preprocessing import preprocess, encode_labels
from functools import partial

# defining constants
EXISTENCE_MODEL_LABELS = 2
POLARITY_MODEL_LABELS = 3
EMOTION_MODEL_LABELS = 7

config = HeirarchicalModelConfig.from_file("./config.yaml")

data = pd.read_csv("./mega data.csv")
data = data[["fetched_tweet", "label"]]
data.rename(columns={"fetched_tweet": "text"}, inplace=True)
data = encode_labels(data)
data.dropna(inplace=True)

if config.apply_preprocessing:
    f = partial(preprocess, **config.preprocessing_settings)
    data.text = data.text.apply(f)

if config.sentic_computing:
    print("Processing sentic net")
    data["sentic_score"] = data.text.apply(sentic_computing_score)

print(data.sample(10))

train = pd.DataFrame()
test = pd.DataFrame()

train, test = train_test_split(data, test_size=0.2, random_state=42, stratify=data.label)

train.reset_index(inplace=True, drop=True)
test.reset_index(inplace=True, drop=True)

Xtrain, Xval = train["text"], test["text"]
existence_ytrain, existence_yval, = (
    train["emotional"],
    test["emotional"],
)
sentimental_ytrain, sentimental_yval = train["encoded_polarity"], test["encoded_polarity"]
emotional_ytrain, emotional_yval = train["encoded_labels"], test["encoded_labels"]

if config.sentic_computing:
    sentic_scores_train, sentic_scores_val = train["sentic_score"], test["sentic_score"]
    config.sentic_scores_train = sentic_scores_train
    config.sentic_scores_validate = sentic_scores_val

config.existence_data_train = (Xtrain, existence_ytrain)
config.existence_data_validate = (Xval, existence_yval)

config.sentiment_data_train = (Xtrain, sentimental_ytrain)
config.sentiment_data_validate = (Xval, sentimental_yval)

config.emotion_data_train = (Xtrain, emotional_ytrain)
config.emotion_data_validate = (Xval, emotional_yval)

# baseline training
base_model_names = [
    # "google/muril-base-cased",
    # "bert-base-uncased",
    # "roberta-base",
    # "albert-base-v2",
    "distilbert-base-uncased",
    # "xlm-roberta-base",
    # "cardiffnlp/twitter-roberta-base"
]

mlflow.set_tracking_uri(config.mlflow_args["tracking uri"])
for model_name in base_model_names:
    os.environ["MLFLOW_EXPERIMENT_NAME"] = config.mlflow_args["experiment name"]
    mlflow.set_experiment(experiment_name=config.mlflow_args["experiment name"])
    with mlflow.start_run(
        run_name=model_name,
        tags={
            "preprocessing": config.apply_preprocessing,
            "SenticNet": config.sentic_computing,
            "EarlyStop": True,
            "tokenization": "Default",
        },
    ):
        if config.apply_preprocessing:
            mlflow.log_params(config.preprocessing_settings)
        # logging dataset to the run
        with tempfile.TemporaryDirectory() as file:
            data.to_csv("data.csv", index=False)
            mlflow.log_artifact("data.csv", "")
            os.remove("data.csv")

        tokenizer = AutoTokenizer.from_pretrained(model_name)

        encodings_train = tokenizer.batch_encode_plus(
            Xtrain,
            max_length=config.max_length,
            padding="max_length",
            return_token_type_ids=True,
            truncation=True,
        )
        training_set = Triage(encodings_train["input_ids"], encodings_train["attention_mask"], emotional_ytrain)
        encodings_test = tokenizer.batch_encode_plus(
            Xval,
            max_length=config.max_length,
            padding="max_length",
            return_token_type_ids=True,
            truncation=True,
        )
        testing_set = Triage(encodings_test["input_ids"], encodings_test["attention_mask"], emotional_ytrain)

        b_size = config.emotion_model_training_args.per_device_train_batch_size
        training_loader = DataLoader(training_set, batch_size=b_size, shuffle=True, num_workers=0)
        testing_loader = DataLoader(testing_set, batch_size=b_size, shuffle=True, num_workers=0)
        training_args = config.emotion_model_training_args

        model = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=7)
        assert training_args is not None
        trainer = Trainer(
            model=model,
            args=training_args,
            train_dataset=training_set,
            eval_dataset=testing_set,
            tokenizer=tokenizer,
            compute_metrics=compute_multiclass_metrics,
        )

        trainer.train()  # type: ignore

        print(summary(model, zeros((1, 256), dtype=torch.int64).to("cuda"), show_input=True))
