from torch.utils.data import Dataset
import torch


class CustomDataset(Dataset):
    def __init__(self, inputs, labels=None, masks=None):
        self.labels = labels
        self.inputs = inputs
        self.masks = masks

    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, idx):
        return {"input_ids": self.inputs[idx], "labels": self.labels[idx], "masks": self.masks[idx]}


class Triage(Dataset):
    def __init__(self, ids, masks, labels=None, sentic_values=None):
        self.ids = ids
        self.masks = masks
        self.labels = labels
        if sentic_values is None:
            print("Sentic value is none")
            self.sentic_values = [0] * len(self.ids)
        else:
            print("Sentic value is not none")
            self.sentic_values = sentic_values
        # print(f'Length of sentic values and ids is same: {len(self.ids) == len(self.sentic_values)}')

    def __getitem__(self, index):
        if self.labels is not None:
            return {
                "input_ids": torch.tensor(self.ids[index], dtype=torch.long),
                "attention_mask": torch.tensor(self.masks[index], dtype=torch.long),
                "labels": torch.tensor(self.labels[index], dtype=torch.long),
                "sentic_value": torch.tensor(self.sentic_values[index], dtype=torch.float),
            }
        else:
            return {
                "input_ids": torch.tensor(self.ids[index], dtype=torch.long),
                "attention_mask": torch.tensor(self.masks[index], dtype=torch.long),
                "sentic_value": torch.tensor(self.sentic_values[index], dtype=torch.float),
            }

    def __len__(self):
        return len(self.ids)
