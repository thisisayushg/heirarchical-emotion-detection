from sklearn.preprocessing import LabelEncoder
import pickle
import os
from wordsegment import load, segment
import emoji
import re
from string import punctuation

load()

TWEET_SHORTURL_REGEX = r"https:?\/\/t.co\/([a-zA-Z0-9]+)"
URL_REGEX = r"https?:\/\/(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+"
TWITTER_USERNAME_REGEX = r"(?<=^|(?<=[^a-zA-Z0-9-_\.]))(@[_A-Za-z]+[A-Za-z0-9-_]+)"
REPITITIVE_GROUP_REGEX = r"([^a-zA-Z0-9_]+?)\1+"
HASHTAG_REGEX = r"\B#(\w*[A-Za-z_]+\w*)"

COMMON_ABBR = {
    "lol": "laughing out loud",
    "rofl": "Rolling on the floor laughing",
    "btw": "by the way",
    "brb": "be right back",
    "lmao": "laughing my ass off",
    "omg": "oh my god",
    "bff": "best friends forever",
    "eod": "end of the day",
    "gtg": "got to go",
    "tl;dr": "too long dont read",
    "ikr": "i know right",
    "idk": "i dont know",
    "diy": "do it yourself",
    "afaik": "as far as i know",
    "asap": "as soon as possible",
    "np": "no problem",
    "ty": "thank you",
    "icymi": "in case you missed it",
    "ttyl": "talk to you later",
    "imo": "in my opinion",
    "tbd": "to be discussed",
    "nvm": "never mind",
    "ily": "i love you",
    "tbh": "to be honest",
    "faq": "frequently asked questions",
    "lmk": "let me know",
    "ofc": "office",
    "dm": "direct message",
    "2moro": "tomorrow",
    "2nite": "tonight",
    "brb": "be right back",
    "btw": "by the way",
    "gr8": "great",
    "irl": "in real life",
    "JK": "just kidding",
    "oic": "oh, i see",
    "pov": "point of view",
    "rbtl": "read between the lines",
    "rt": "retweet",
    "thx": "thanks",
    "tyvm": "Thank You Very Much",
    "wtf": "what the fuck",
    "wywh": "wish you were here",
    "xoxo": "hugs and kisses",
    "sd": "sweet dreams",
    "tc": "take care",
    "fyi": "for your information",
    "jsyn": "just so you know",
    "n": "and",
}

HINDI_WORD_MAP = {
    "h": "hai",
    "ni": "nahi",
    "ny": "nahi",
    "nai": "nahi",
    "q": "kyu",
    "ha": "haan",
    "m": "main",
    "kr": "kar",
    "krte": "karte",
    "karra": "kar raha",
    "rahey": "rahe",
    "rae": "rahe",
    "b": "bhi",
    "v": "bhi",
    "pr": "pe",
    "per": "pe",
    "py": "pe",
    "toh": "to",
    "woh": "wo",
    "unn": "un",
    "u": "you",
    "hv": "have",
    "chor": "chhod",
    "chhor": "chhod",
    "kuch": "kuchh",
    "mtlb": "matlab",
    "aj": "aaj",
    "mazze": "maze",
    "majje": "maze",
    "maje": "maze",
    "agay": "aage",
    "phr": "phir",
    "fir": "phir",
    "bht": "bahut",
}


def sentiment_assignment(x):
    try:
        if x.lower() == "happiness" or x.lower() == "surprise":
            return "positive"
        elif x.lower() == "neutral":
            return "neutral"
        else:
            return "negative"
    except Exception:
        print(x)


def encode_labels(data):
    data["emotional"] = data.label.apply(lambda x: 0 if x == "neutral" else 1)
    # print(data["emotional"].value_counts())

    assert data[data["label"].isna()].shape[0] == 0

    data["sentiment"] = data["label"].apply(sentiment_assignment)
    data["polarity"] = data["sentiment"].map({"positive": 1, "negative": -1, "neutral": 0})

    data["encoded_labels"] = (
        data["label"]
        .str.lower()
        .map({"neutral": 0, "anger": 1, "fear": 2, "surprise": 3, "disgust": 4, "sadness": 5, "happiness": 6})
    )
    data["encoded_polarity"] = data["polarity"].map({0: 0, -1: 1, 1: 2})

    return data


def preprocess(
    text: str,
    segmentation=True,
    remove_link=True,
    replace_emoji=True,
    remove_username=True,
    replace_abbr=True,
    standardize_hindi_typo=True,
):
    if standardize_hindi_typo:
        replace_emoji = True
        remove_link = True
        remove_username = True
    # lowercase
    text = text.lower()

    # removal of t.co short url
    text = re.sub(TWEET_SHORTURL_REGEX, "", text)

    text = text.replace("..", " ")
    text = text.replace("...", " ")

    if remove_link:
        # removal of url
        text = re.sub(URL_REGEX, "", text)

    if remove_username:
        # removal of usernames
        text = re.sub(TWITTER_USERNAME_REGEX, "", text)

    if segmentation:
        # hashtag segmentation
        new_text = ""
        for word in text.split():
            if re.match(HASHTAG_REGEX, word) is not None:
                new_text += " ".join(segment(word))
                new_text += " "
            else:
                new_text += word + " "
        text = new_text

    if replace_emoji:
        # replace repititive items like '~~~' or !!! or 😁😁😁😁😁😁, which dont match a character or a number, with single occurence
        matches = re.finditer(REPITITIVE_GROUP_REGEX, text)
        for match in matches:
            try:
                text = text.replace(match.group(0), match.group(1))
            except Exception:
                continue

        # replace emoji with their text descriptions
        text = emoji.demojize(text).replace(":", " ")

    if replace_abbr:
        # replace common chat abbreviations
        for abbr, expansion in COMMON_ABBR.items():
            text = re.sub(r"(\s|^){abbr}(\s|$)", " {expansion} ", text)

    if standardize_hindi_typo:
        for curr, next in HINDI_WORD_MAP.items():
            text = re.sub(f" {curr} ", f" {next} ", text)
        sanitized_str = []
        for token in text.split():
            norm_token = _normalize(token)
            if norm_token.endswith("ney") or norm_token.endswith("key"):
                sanitized_str.append(norm_token[:-1])
            elif norm_token.endswith("ny"):
                sanitized_str.append(norm_token[:-2] + "ne")
            else:
                sanitized_str.append(norm_token)
        text = " ".join(sanitized_str)

    return text


def _normalize(word: str):
    """
    Function to normalize a given word, i.e to remove spaces, remove punctuation marks
    """
    word = word.strip().lower()
    normalized_word = ""
    for char in word:
        if char not in punctuation:
            normalized_word += char
    return normalized_word
