from transformers import Trainer
import mlflow
from time import time
import tempfile
from artifact_manager import ArtifactManager
import os
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score
import numpy as np
import torch
from torch import Tensor
from torch.optim import Optimizer
from typing import Union
from mlflow.


def compute_binary_metrics(p):
    pred, labels = p
    # pred is a tupe, pred[0] is value of array probs and pred[1] is dtype

    if isinstance(pred, tuple):
        pred = pred[0]

    accuracy = accuracy_score(y_true=labels, y_pred=pred)
    recall = recall_score(y_true=labels, y_pred=pred)
    precision = precision_score(y_true=labels, y_pred=pred)
    f1 = f1_score(y_true=labels, y_pred=pred)
    return {
        "accuracy": accuracy,
        "precision": precision,
        "recall": recall,
        "f1": f1,
    }


def compute_multiclass_metrics(p, prob_based=True):
    pred, labels = p
    # pred is a tupe, pred[0] is value of array probs and pred[1] is dtype
    if isinstance(pred, tuple):
        pred = pred[0]
    if prob_based:
        pred = np.argmax(pred, axis=1)
    accuracy = accuracy_score(y_true=labels, y_pred=pred)
    recall = recall_score(y_true=labels, y_pred=pred, average="macro")
    precision = precision_score(y_true=labels, y_pred=pred, average="macro")
    f1 = f1_score(y_true=labels, y_pred=pred, average="macro")
    return {
        "accuracy": accuracy,
        "precision": precision,
        "recall": recall,
        "f1": f1,
    }


def save_model_and_data(trainer: Union[Trainer, torch.nn.Module], model_type, num_labels):
    manager = ArtifactManager()
    if isinstance(trainer, Trainer):
        manager.save_model(trainer, "huggingface_model")
    elif isinstance(trainer, torch.nn.Module):
        manager.save_model(trainer, "huggingface_model")
    active_run = mlflow.active_run()
    if active_run is not None:
        curr_run_id = active_run.info.run_id
        curr_exp_id = active_run.info.experiment_id
        with tempfile.TemporaryDirectory() as tmp:
            path = os.path.join(tmp, "model_mapping.txt")
            with open(path, "w") as file:
                file.write(f"{curr_run_id},{model_type},{num_labels}")
            mlflow.log_artifact(path, "")
        mlflow.pyfunc.save_model(
            path=str(os.path.join("mlruns", curr_exp_id, curr_run_id, "artifacts", "mlflow_model")),
            python_model=manager,
        )


def log_metrics(metrics, loss, start_time, n_examples, step, prefix="eval_"):
    duration = time() - start_time
    metrics["loss"] = loss
    metrics["runtime"] = duration
    metrics["samples_per_second"] = round(n_examples / duration, 2)
    metrics["steps_per_second"] = metrics["steps_per_second"] / duration
    metrics = {f"{prefix}{key}": float(value) for key, value in metrics.items()}
    mlflow.log_metrics(metrics, step=step)
    return metrics


def log_params(trainer: Trainer, log_basemodel_params=False):
    model = trainer.model
    args = trainer.args

    combined_dict = args.to_dict()
    if log_basemodel_params:
        combined_dict = {**model.base_model.config.to_dict(), **combined_dict}
    if hasattr(model, "config") and model.config is not None:
        model_config = model.config.to_dict()
        combined_dict = {**model_config, **combined_dict}

    # combined_dict = flatten_dict(combined_dict) if self._flatten_params else combined_dict
    # remove params that are too long for MLflow
    for name, value in list(combined_dict.items()):
        # internally, all values are converted to str in MLflow
        if len(str(value)) > 250:
            print(
                f'Trainer is attempting to log a value of "{value}" for key "{name}" as a parameter. MLflow\'s'
                " log_param() only accepts values no longer than 250 characters so we dropped this attribute."
                " You can use `MLFLOW_FLATTEN_PARAMS` environment variable to flatten the parameters and"
                " avoid this message."
            )
            del combined_dict[name]
    # MLflow cannot log more than 100 values in one go, so we have to split it
    combined_dict_items = list(combined_dict.items())
    for i in range(0, len(combined_dict_items), 100):
        mlflow.log_params(dict(combined_dict_items[i : i + 100]))


def move_to_device(batch_data, device):
    ids = batch_data["input_ids"].to(device, dtype=torch.long)
    mask = batch_data["attention_mask"].to(device, dtype=torch.long)
    targets = batch_data["labels"].to(device, dtype=torch.long)
    sentic_value = batch_data["sentic_value"].to(device, dtype=torch.float)
    return ids, mask, targets, sentic_value


def predict_from_model(model, loss_function, targets, *inputs):
    outputs = model(*inputs)
    loss = loss_function(outputs, targets)
    _, preds = torch.max(outputs.data, dim=1)
    return preds, loss


def backpropagate(loss: Tensor, optimizer: Optimizer):
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()


class SaveBestModel:
    """
    Class to save the best model while training. If the current epoch's
    validation loss is less than the previous least less, then save the
    model state.
    """

    def __init__(self, model_type, optimization_metric="f1"):
        self.best = None
        self.model_type = model_type
        self.optimization_metric = optimization_metric
        if "loss" in optimization_metric:
            self.better = lambda a, best: a < best
        else:
            self.better = lambda a, best: a > best
        self.best_model_path = ""

    def __call__(self, metrics, epoch, model):
        if self.best is None:
            self.best = metrics[self.optimization_metric]
            return

        if np.isnan(metrics[self.optimization_metric]):
            return

        if self.better(metrics[self.optimization_metric], self.best):
            self.best = metrics[self.optimization_metric]
            print(f"\nBest {self.optimization_metric}: {self.best}")
            print(f"\nSaving best model for epoch: {epoch+1}\n")
            active_run = mlflow.active_run()
            if active_run is not None:
                artifact_uri = active_run.info.artifact_uri
                file_path = os.path.join(artifact_uri, self.model_type)
                os.makedirs(file_path, exist_ok=True)
                file_path = os.path.join(file_path, "best_model.pth")
                torch.save(
                    {
                        "optimization_metric": self.best,
                        "epoch": epoch + 1,
                        "model_state_dict": model.state_dict(),
                    },
                    file_path,
                )
                self.best_model_path = file_path

        return self.best_model_path
