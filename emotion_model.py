from torch import nn, Tensor, sum, rand, unsqueeze, cat, mean


class Attention(nn.Module):
    def __init__(self, encoder_dim):
        super().__init__()
        self.encoder_dim = encoder_dim
        self.bias = nn.parameter.Parameter(Tensor(1))
        self.W1 = nn.parameter.Parameter(nn.init.kaiming_uniform_(rand(self.encoder_dim, 1)))

    def forward(self, values: Tensor, sentic_values: Tensor):
        alignment_scores = values @ self.W1
        # including senticnet
        alignment_scores = alignment_scores + sentic_values
        alignment_scores = nn.Softmax(dim=0)(alignment_scores)

        weighted_values = alignment_scores * values
        context_vector = mean(weighted_values.float(), dim=1)
        return context_vector


class Expand(nn.Module):
    def __init__(self, size: int):
        super(Expand, self).__init__()
        self.size = size

    def forward(self, values: Tensor):
        values = unsqueeze(values, 1)
        values = values.repeat(1, self.size, 1)
        return values


class Concat(nn.Module):
    def __init__(self, dim: int):
        super(Concat, self).__init__()
        self.dim = dim

    def forward(self, tensor1: Tensor, tensor2: Tensor):
        return cat((tensor1, tensor2), dim=self.dim)


class EmotionModel(nn.Module):
    def __init__(self, num_labels, base_model, config=None):
        super(EmotionModel, self).__init__()
        self.num_labels = num_labels
        self.base_model = base_model
        basemodel_config = self.base_model.config
        self.attention = Attention(basemodel_config.hidden_size)
        self.flatten = (
            nn.Flatten()
        )  # start_dim = 1 since 0 is batch_size, end dim=-1 since you need to flatten for all dimensions
        self.classifier = nn.Linear(128, num_labels)  # load and initialize weights
        self.relu = nn.ReLU()
        if config is not None:
            self.max_length = config.max_length
            self.linear = nn.Linear(config.max_length * basemodel_config.hidden_size * 2, 128)
            self.expand = Expand(config.max_length)
        self.concat = Concat(dim=2)

    def forward(self, input_ids=None, attention_mask=None, support_outputs=None, sentic_value=0):
        """
        support outputs are output predictions for each sentence in the batch by existence and sentiment model combined
        """
        outputs = self.base_model(input_ids=input_ids, attention_mask=attention_mask)
        last_hidden_state = outputs.hidden_states[-1]
        max_length = last_hidden_state.size(1)
        embedding_size = last_hidden_state.size(2)
        print(f"sentic value is {sentic_value}")
        if support_outputs is not None:
            tiled_output = (
                support_outputs.unsqueeze(1)
                .repeat_interleave(max_length * embedding_size)
                .reshape(-1, max_length, embedding_size)
            )
            last_hidden_state = tiled_output * last_hidden_state
            last_hidden_state = last_hidden_state.float()

        # including sentic net
        sentic_vector = sentic_value.reshape(-1, 1).repeat(1, self.max_length).reshape(-1, self.max_length, 1)
        sentic_vector = nn.Tanh()(sentic_vector)

        context_vector = self.attention(last_hidden_state, sentic_vector)
        context_vector = self.relu(context_vector)
        context_vector = self.expand(context_vector)

        concatenated_tensor = self.concat(last_hidden_state, context_vector)

        # input to MLP
        x = self.flatten(concatenated_tensor)
        x = self.linear(x)
        x = self.relu(x)
        logits = self.classifier(x)
        return logits


class ExistenceModel(nn.Module):
    def __init__(self, num_labels, base_model, hidden_dim=1024):
        super(ExistenceModel, self).__init__()
        self.num_labels = num_labels
        self.base_model = base_model
        basemodel_config = self.base_model.config
        # GRU layer
        self.gru = nn.GRU(basemodel_config.hidden_size, hidden_dim, num_layers=1, bidirectional=True, batch_first=True)
        self.flatten = (
            nn.Flatten()
        )  # start_dim = 1 since 0 is batch_size, end dim=-1 since you need to flatten for all dimensions
        self.classifier = nn.Linear(128, num_labels)  # load and initialize weights
        self.relu = nn.ReLU()
        self.linear = nn.Linear(2 * hidden_dim, 128)  # 2 since bidirectional outputs will be concatenated
        self.concat = Concat(dim=1)

    def forward(self, input_ids=None, attention_mask=None):
        """
        support outputs are output predictions for each sentence in the batch by existence and sentiment model combined
        """
        outputs = self.base_model(input_ids=input_ids, attention_mask=attention_mask)
        last_hidden_state = outputs.hidden_states[-1]

        # input to MLP
        packed_output, hidden = self.gru(last_hidden_state)
        # concat the final forward and backward hidden state
        x = self.concat(hidden[-2, :, :], hidden[-1, :, :])
        x = self.flatten(x)
        x = self.linear(x)
        x = self.relu(x)
        logits = self.classifier(x)
        return logits
